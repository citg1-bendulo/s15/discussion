package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;

public interface PostService {


    void createPost(String stringToken, Post post);
    Iterable<Post> getPosts();
    ResponseEntity deletePost(String token,Long id);

    ResponseEntity updatePost(String token,Long id,Post post);

    Iterable<Post> getMyPosts(String stringToken);
}
